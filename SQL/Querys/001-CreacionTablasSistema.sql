-- Autor Dev: Andres Leyton
-- Descripci�n: Creaci�n tablas prueba desarrollo Ingeneo.
-- FechaCreacion: 23/09/2021
---***************************************************
-- *** PASO 1 ***
CREATE TABLE Cliente(
	ID_Cliente INT IDENTITY(1,1) PRIMARY KEY NOT NULL
	, Documento VARCHAR(15) NOT NULL
	, NombreCliente VARCHAR (80) NOT NULL
	, CorreoElectronico VARCHAR(40) NOT NULL
	, DireccionDomicilio VARCHAR(100) NOT NULL
	, Telefono VARCHAR(10) NULL
	, Celular VARCHAR(11) NULL
	, FechaRegistro DATETIME NOT NULL
)
GO
-- *** PASO 2 ***
CREATE TABLE TipoLogistica(
	ID_TipoLogistica INT IDENTITY(1,1) PRIMARY KEY NOT NULL
	, TipoLogistica VARCHAR(50) NOT NULL
	, FechaRegistro DATETIME NOT NULL
)
GO
-- *** PASO 3 ***
CREATE TABLE TipoDescuentoOtorgado(
	ID_TipoDescuento INT IDENTITY(1,1) PRIMARY KEY NOT NULL
	, TipoDescuento VARCHAR(50) NOT NULL
	, Descuento SMALLINT NOT NULL
	, FechaRegistro DATETIME NOT NULL
)
GO
-- *** PASO 4 ***
CREATE TABLE LogisticaVehiculo (
	ID INT IDENTITY(1,1) PRIMARY KEY NOT NULL
	, ID_Cliente INT REFERENCES Cliente(ID_Cliente) NOT NULL
	, ID_TipoLogistica INT REFERENCES TipoLogistica (ID_TipoLogistica) NOT NULL
	, TipoProducto VARCHAR(100) NOT NULL
	, CantidadProducto INT NOT NULL
	, FechaRegisto DATETIME NOT NULL
	, FechaEntrega DATETIME NOT NULL
	, BodegaEntrega VARCHAR(50) NOT NULL
	, PrecioEnvio DECIMAL(18,2) NOT NULL
	, ID_TipoDescuento INT REFERENCES TipoDescuentoOtorgado(ID_TipoDescuento) NOT NULL
	, PlacaVehiculo NVARCHAR(6) NOT NULL
	, NumeroGuia NVARCHAR(10) NOT NULL
)
GO
-- *** PASO 5 ***
CREATE TABLE LogisticaMaritima (
	ID INT IDENTITY(1,1) PRIMARY KEY NOT NULL
	, ID_Cliente INT REFERENCES Cliente(ID_Cliente) NOT NULL
	, ID_TipoLogistica INT REFERENCES TipoLogistica (ID_TipoLogistica) NOT NULL
	, TipoProducto VARCHAR(100) NOT NULL
	, CantidadProducto INT NOT NULL
	, FechaRegisto DATETIME NOT NULL
	, FechaEntrega DATETIME NOT NULL
	, BodegaEntrega VARCHAR(50) NOT NULL
	, PrecioEnvio DECIMAL(18,2) NOT NULL
	, ID_TipoDescuento INT REFERENCES TipoDescuentoOtorgado(ID_TipoDescuento) NOT NULL
	, NumeroFlota NVARCHAR(7) NOT NULL
	, NumeroGuia NVARCHAR(10) NOT NULL
)