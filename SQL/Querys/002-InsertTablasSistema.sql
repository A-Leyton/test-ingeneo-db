-- Autor Dev: Andres Leyton
-- Descripci�n: Insert en tablas relacionales
-- FechaCreacion: 23/09/2021
---***************************************************
INSERT INTO TipoLogistica
	(TipoLogistica
	,FechaRegistro)
VALUES
	('Veh�culo'
	, GETDATE())
GO
INSERT INTO TipoLogistica
	(TipoLogistica
	,FechaRegistro)
VALUES
	('Maritimo'
	, GETDATE())
---------------------------------------------------------
--*******************************************************
GO
INSERT INTO TipoLogistica
	(TipoLogistica
	,FechaRegistro)
VALUES
	('Veh�culo'
	, GETDATE())
GO
INSERT INTO TipoDescuentoOtorgado
	(TipoDescuento
	,Descuento
	, FechaRegistro)
VALUES
	('Terrestre'
	, 5
	, GETDATE())
GO
INSERT INTO TipoDescuentoOtorgado
	(TipoDescuento
	,Descuento
	, FechaRegistro)
VALUES
	('Maritimo'
	, 3
	, GETDATE())
---------------------------------------------------------
--*******************************************************